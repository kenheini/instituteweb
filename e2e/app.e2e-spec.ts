import { EdumywebappPage } from './app.po';

describe('edumywebapp App', () => {
  let page: EdumywebappPage;

  beforeEach(() => {
    page = new EdumywebappPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
