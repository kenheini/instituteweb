import { Component } from '@angular/core';
import { SharedService } from '../../../UtilityApp/Services/shared.service';

@Component({
  selector: 'studentlist',
  templateUrl: './studentlist.component.html'
})
export class StudentListComponent {
  //title = 'app works!';
  constructor(private shared:SharedService){
          this.shared.emitTypeBroadcast();
        }
}
