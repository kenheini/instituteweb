import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { LandingPageComponent } from './AccountApp/LandingPage/landingPage.component';
import { SignupComponent } from './AccountApp/SignupPage/signup.component';
import { LoginComponent } from './AccountApp/LoginPage/login.component';
import { HeaderComponent } from './UtilityApp/Header/header.component';
import { SidebarComponent } from './UtilityApp/Sidebar/sidebar.component';
import { FooterComponent } from './UtilityApp/Footer/footer.component';
import { NavigationComponent } from './UtilityApp/Navigation/navigation.component';
import { TopnavbarComponent } from './UtilityApp/Topnavbar/topnavbar.component';
import { UserMasterComponent } from './AdminApp/UserMaster/usermaster.component';
import { ConfirmMobileComponent } from './AccountApp/ConfirmMobile/confirm-mobile.component';
//import { AddEmployeeComponent } from './UtilityApp/AddEmployee/addemployee.component';


//Dashboard & internal pages
import { DashboardComponent } from './DashboardApp/Dashboard/dashboard.component'
import { AttendanceComponent } from './DashboardApp/Attendance/attendance.component';
import { AddEmployeeComponent } from './DashboardApp/Employees/Add/addemployee.component';
import { ManageEmployeeComponent } from './DashboardApp/Employees/manage/manageemployee.component';
import { FinanceAssetManagementComponent } from './DashboardApp/Finance/AssetManagement/financeassetmanagement.component';
import { FinanceDonationComponent } from './DashboardApp/Finance/Donations/financedonation.component';
import { FinanceFeeComponent } from './DashboardApp/Finance/Fees/financefee.component';
import { FinancePayslipComponent } from './DashboardApp/Finance/Payslip/financepayslip.component';
import { FinanceReportComponent } from './DashboardApp/Finance/Report/financereport.component';
import { FinanceCategoryComponent } from './DashboardApp/Finance/Category/financecategory.component';
import { FinanceTransactionComponent } from './DashboardApp/Finance/Transaction/financetransaction.component';
import { HostelAdditionDetailComponent } from './DashboardApp/Hostel/AdditionalDetail/hosteladditionaldetail.component';
import { HostelFeeCollectionComponent } from './DashboardApp/Hostel/FeeCollection/hostelfeecollection.component';
import { HostelFeeDefaulterComponent } from './DashboardApp/Hostel/FeeDefaulter/hostelfeedefaulter.component';
import { HostelFeePayComponent } from './DashboardApp/Hostel/FeePay/hostelfeepay.component';
import { HostelReportComponent } from './DashboardApp/Hostel/Report/hostelreport.component';
import { HostelRoomAdditionalDetailComponent } from './DashboardApp/Hostel/RoomAdditionalDetail/hostelroomadditionaldetail.component';
import { HostelRoomAllocationComponent } from './DashboardApp/Hostel/RoomAllocation/hostelroomallocation.component';
import { AddSchoolComponent } from './DashboardApp/School/Add/addschool.component';
import { SchoolListComponent } from './DashboardApp/School/List/schoollist.component';
import { SettingsComponent } from './DashboardApp/Settings/settings.component';
import { AcadamicSettingsComponent } from './DashboardApp/Settings/Acadamic/acadamicsettings.component';
import { EmployeeSettingsComponent } from './DashboardApp/Settings/Employee/employeesettings.component';
import { GeneralSettingsComponent } from './DashboardApp/Settings/General/generalsettings.component';
import { PaymentSettingsComponent } from './DashboardApp/Settings/Payment/paymentsettings.component';
import { StudentSettingsComponent } from './DashboardApp/Settings/Student/studentsettings.component';
import { AddStudentComponent } from './DashboardApp/Students/Add/addstudent.component';
import { StudentListComponent } from './DashboardApp/Students/List/studentlist.component';
import { TimetableComponent } from './DashboardApp/TimeTable/timetable.component';
import { TransportFeeComponent } from './DashboardApp/Transport/Fee/transportfee.component';
import { TransportManageRouteComponent } from './DashboardApp/Transport/ManageRoute/transportmanageroute.component';
import { TransportManageVehicleComponent } from './DashboardApp/Transport/ManageVehicle/transportmanagevehicle.component';
import { TransportReportComponent } from './DashboardApp/Transport/Report/transportreport.component';
import { TransportSetRouteComponent } from './DashboardApp/Transport/SetRoutes/transportsetroute.component';



//Service
import { AccountService } from './AccountApp/Services/account.service'
import { LoginService } from './AccountApp/Services/login.service'
import { SignupService } from './AccountApp/Services/signup.service'
import { ConfirmMobleService } from './AccountApp/Services/confirm-mobile.service'


import { LocalstorageService } from './UtilityApp/Services/localstorage.service';
import { SharedService } from './UtilityApp/Services/shared.service';
import { NotificationService } from './UtilityApp/Services/notification.service';
import { Broadcaster } from './UtilityApp/Services/broadcaster';
import { MessageEvent } from './UtilityApp/Services/message_event';

import { Http, XHRBackend, RequestOptions } from '@angular/http';
import { HttpService } from './UtilityApp/Services/http.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SidebarComponent,
    FooterComponent,
    NavigationComponent,
    TopnavbarComponent,
    DashboardComponent,
    UserMasterComponent,
    LandingPageComponent,
    SignupComponent,
    LoginComponent,
    ConfirmMobileComponent,
    AddEmployeeComponent,
    AddSchoolComponent,
    DashboardComponent,    
    AttendanceComponent,
    AddEmployeeComponent,
    ManageEmployeeComponent,
    FinanceAssetManagementComponent,
    FinanceDonationComponent,
    FinanceFeeComponent,
    FinancePayslipComponent,
    FinanceReportComponent,
    FinanceCategoryComponent,
    FinanceTransactionComponent,
    HostelAdditionDetailComponent,
    HostelFeeCollectionComponent,
    HostelFeeDefaulterComponent,
    HostelFeePayComponent,
    HostelReportComponent,
    HostelRoomAdditionalDetailComponent,
    HostelRoomAllocationComponent,  
    SchoolListComponent,
    SettingsComponent,
    AcadamicSettingsComponent,
    EmployeeSettingsComponent,
    GeneralSettingsComponent,
    PaymentSettingsComponent,
    StudentSettingsComponent,
    AddStudentComponent,
    StudentListComponent,
    TimetableComponent,
    TransportFeeComponent,
    TransportManageRouteComponent,
    TransportManageVehicleComponent,
    TransportReportComponent,
    TransportSetRouteComponent

  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [
    AccountService,
    LocalstorageService,
    LoginService,
    SignupService,
    ConfirmMobleService,
    SharedService,
    Broadcaster,
    MessageEvent,
    NotificationService, {
      provide: Http,
      useFactory: httpFactory,
      deps: [XHRBackend, RequestOptions]
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function httpFactory(backend: XHRBackend, options: RequestOptions) {
  return new HttpService(backend, options);
}